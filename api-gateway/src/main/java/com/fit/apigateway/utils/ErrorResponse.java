<<<<<<<< HEAD:api-gateway/src/main/java/com/fit/apigateway/utils/ErrorResponse.java
package com.fit.apigateway.utils;
========
package com.fit.userservice.utils;
>>>>>>>> 6c2e6d42f4a9f47af0dd956bd717b571245eb37e:user-service/src/main/java/com/fit/userservice/utils/ErrorResponse.java

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ErrorResponse {
    private int errorCode;
    private String message;
}

