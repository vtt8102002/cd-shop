package com.fit.Payment_service.Dto;

import lombok.Data;

@Data
public class Customer {

    private Integer id;
    private String ten;
    private String soDienThoai;
    private String matKhau;
}
