package com.fit.Payment_service.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class PaymentDTO {

    private Integer id;
    private Integer soTien;
    private Customer customerID;
    private Date ngayThanhToan;
    private boolean trangThai;
}
