package com.fit.Payment_service.Service;

import com.fit.Payment_service.Dto.Customer;
import com.fit.Payment_service.Dto.PaymentDTO;
import com.fit.Payment_service.Entity.Payment;
import com.fit.Payment_service.Repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PaymentService {
    @Autowired
    private PaymentRepository paymentRepository;

    public Payment createPayment(Payment payment) {
        return paymentRepository.save(payment);
    }

    public List<Payment> getAll() {
        return paymentRepository.findAll();
    }

//    public List<PaymentDTO> getAllPayment() {
//        List<PaymentDTO> list = new ArrayList<PaymentDTO>();
//        paymentRepository.findAll().forEach((s)->{
//            Customer customer = restTemplate.getForObject("http://course-service:8081/course/get/"+s.getCourseId(),Course.class);
//            list.add(new StudentDto(s.getId(),s.getName(),s.getAge(),s.getGender(),course));
//        });
//        return list;
//    }

    public Optional<Payment> getPaymentById(int id) {
        return paymentRepository.findById(id);
    }

    public Payment updatePayment(int id, Payment paymentDetails) {
        Payment payment = paymentRepository.findById(id).orElseThrow();
        payment.setSoTien(paymentDetails.getSoTien());
        payment.setCustomerID(paymentDetails.getCustomerID());
//        payment.setNgayThanhToan(paymentDetails.setNgayThanhToan());
        payment.setTrangThai(paymentDetails.isTrangThai());

        return paymentRepository.save(payment);
    }

    public void deletePayment(int id) {
        paymentRepository.deleteById(id);
    }
}
