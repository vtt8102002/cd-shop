package com.fit.Store_service.Repository;

//import com.fit.CD_service.Entity.CD;
import com.fit.Store_service.Entity.Store;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StoreRepository extends JpaRepository<Store, Integer> {
}
