package com.fit.Store_service.Controller;


import com.fit.Store_service.Entity.Store;
import com.fit.Store_service.Service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/cd")
public class StoreController {
    @Autowired
    private StoreService storeService;

    @PostMapping
    public Store createStore(@RequestBody Store store) {
        return storeService.createStore(store);
    }

    @GetMapping
    public List<Store> getAllStore() {
        return storeService.getAllStore();
    }

    @GetMapping("/getOne")
    public ResponseEntity<Store> getStoreById(@PathVariable int id) {
        Optional<Store> store = storeService.getStoreById(id);
        return store.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/update")
    public ResponseEntity<Store> updateStore(@PathVariable int id, @RequestBody Store storeDetails) {
        return ResponseEntity.ok(storeService.updateStore(id, storeDetails));
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Void> deleteStore(@PathVariable int id) {
        storeService.deleteStore(id);
        return ResponseEntity.noContent().build();
    }
}

