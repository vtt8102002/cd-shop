package com.fit.Store_service.Entity;


import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name="store-db")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Store {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String tenCuaHang;
    private String diaChi;
    private String phone;
}
