package com.fit.Store_service.Service;

//import com.fit.CD_service.Entity.CD;
//import com.fit.CD_service.Repository.CDRepository;
import com.fit.Store_service.Entity.Store;
import com.fit.Store_service.Repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StoreService {
    @Autowired
    private StoreRepository storeRepository;

    public Store createStore(Store store) {
        return storeRepository.save(store);
    }

    public List<Store> getAllStore() {
        return storeRepository.findAll();
    }

    public Optional<Store> getStoreById(int id) {
        return storeRepository.findById(id);
    }

    public Store updateStore(int id, Store storeDetails) {
        Store store = storeRepository.findById(id).orElseThrow();
        store.setTenCuaHang(storeDetails.getTenCuaHang());
        store.setDiaChi(storeDetails.getDiaChi());
        store.setPhone(storeDetails.getPhone());
        return storeRepository.save(store);
    }

    public void deleteStore(int id) {
        storeRepository.deleteById(id);
    }
}
