package com.fit.Order_service.Service;

import com.fit.Order_service.Entity.Order;
import com.fit.Order_service.Repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    public Order createOrder(Order order) {
        return orderRepository.save(order);
    }

    public List<Order> getAll() {
        return orderRepository.findAll();
    }

    public Optional<Order> getOrderById(int id) {
        return orderRepository.findById(id);
    }

    public Order updateOrder(int id, Order orderDetails) {
        Order order = orderRepository.findById(id).orElseThrow();
        order.setNgayBan(orderDetails.getNgayBan());
        order.setCustomerID(orderDetails.getCustomerID());
        order.setCdID(orderDetails.getCdID());
        order.setGia(orderDetails.getGia());

        return orderRepository.save(order);
    }

    public void deleteOrder(int id) {
        orderRepository.deleteById(id);
    }
}
