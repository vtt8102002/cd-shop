package com.fit.Order_service.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class OrderDTO {
    private Integer id;
    private Date ngayBan;
    private Customer customerID;
    private CD cdID;
    private Integer gia;
}
