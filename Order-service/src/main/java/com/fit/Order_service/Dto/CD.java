package com.fit.Order_service.Dto;

import lombok.Data;

@Data
public class CD {
    private Integer id;
    private String ten;
    private String theLoai;
    private Integer gia;
    private Integer soLuong;
}
