package com.fit.CD_service.Service;

import com.fit.CD_service.Entity.CD;
import com.fit.CD_service.Repository.CDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CDService {
    @Autowired
    private CDRepository cdRepository;

    public CD createCD(CD cd) {
        return cdRepository.save(cd);
    }

    public List<CD> getAllCD() {
        return cdRepository.findAll();
    }

    public Optional<CD> getCDById(int id) {
        return cdRepository.findById(id);
    }

    public CD updateCD(int id, CD cdDetails) {
        CD cd = cdRepository.findById(id).orElseThrow();
        cd.setTen(cdDetails.getTen());
        cd.setTheLoai(cdDetails.getTheLoai());
        cd.setGia(cdDetails.getGia());
        cd.setSoLuong(cdDetails.getSoLuong());

        return cdRepository.save(cd);
    }

    public void deleteCD(int id) {
        cdRepository.deleteById(id);
    }
}
