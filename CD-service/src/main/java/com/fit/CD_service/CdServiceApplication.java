package com.fit.CD_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class CdServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CdServiceApplication.class, args);
	}

}
