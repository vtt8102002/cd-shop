package com.fit.CD_service.Entity;


import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name="cd-db")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class CD {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String ten;
    private String theLoai;
    private Integer gia;
    private Integer soLuong;
}
