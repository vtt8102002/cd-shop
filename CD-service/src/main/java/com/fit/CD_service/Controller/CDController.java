package com.fit.CD_service.Controller;

import com.fit.CD_service.Entity.CD;
import com.fit.CD_service.Service.CDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/cd")
public class CDController {
    @Autowired
    private CDService cdService;

    @PostMapping("/createCD")
    public CD createCD(@RequestBody CD cd) {
        return cdService.createCD(cd);
    }

    @GetMapping("/getAllCD")
    public List<CD> getAllCD() {
        return cdService.getAllCD();
    }

    @GetMapping("/getCDByIdvv/{id}")
    public ResponseEntity<CD> getCDById(@PathVariable int id) {
        Optional<CD> cd = cdService.getCDById(id);
        return cd.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/updateCD/{id}")
    public ResponseEntity<CD> updateCD(@PathVariable int id, @RequestBody CD cdDetails) {
        return ResponseEntity.ok(cdService.updateCD(id, cdDetails));
    }

    @DeleteMapping("/deleteCD/{id}")
    public ResponseEntity<Void> deleteCD(@PathVariable int id) {
        cdService.deleteCD(id);
        return ResponseEntity.noContent().build();


    }
}

