package com.fit.CD_service.Repository;

import com.fit.CD_service.Entity.CD;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CDRepository extends JpaRepository<CD, Integer> {
}
