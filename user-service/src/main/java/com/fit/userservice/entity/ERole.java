package com.fit.userservice.entity;

public enum ERole {
    CUSTOMER,
    EMPLOYEE,
    ADMIN
}

