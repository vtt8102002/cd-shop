package com.fit.customer_service.Service;


import java.util.List;

import com.fit.customer_service.Entity.CustomerEntity;
import com.fit.customer_service.Repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    public CustomerEntity createCustomer(CustomerEntity customer) {
        return customerRepository.save(customer);
    }

    public List<CustomerEntity> getAllCustomers() {
        return customerRepository.findAll();
    }

    public Optional<CustomerEntity> getCustomerById(int id) {
        return customerRepository.findById(id);
    }

    public CustomerEntity updateCustomer(int id, CustomerEntity customerDetails) {
        CustomerEntity customer = customerRepository.findById(id).orElseThrow();
        customer.setTen(customerDetails.getTen());
        customer.setSoDienThoai(customerDetails.getSoDienThoai());
        customer.setEmail(customerDetails.getEmail());
        return customerRepository.save(customer);
    }

    public void deleteCustomer(int id) {
        customerRepository.deleteById(id);
    }
}
